import React from 'react';
import { Button, Select } from 'antd';
const { Option } = Select;

const pageSizes = [10, 20, 30, 40, 50];

const Pagination = ({
  setPageSize,
  pageSize,
  pageOptions,
  pageIndex,
  pageCount,
  canPreviousPage,
  canNextPage,
  gotoPage,
  previousPage,
  nextPage,
}) => {
  return (
    <div style={{ marginBottom: 20 }}>
      <Button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
        {'<<'}
      </Button>{' '}
      <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
        {'<'}
      </Button>{' '}
      <Button onClick={() => nextPage()} disabled={!canNextPage}>
        {'>'}
      </Button>{' '}
      <Button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
        {'>>'}
      </Button>{' '}
      <span>
        Страница{' '}
        <strong>
          {pageIndex + 1} из {pageOptions.length}
        </strong>{' '}
      </span>
      <Select
        style={{ width: '100px' }}
        value={pageSize}
        onChange={value => {
          setPageSize(Number(value));
        }}
      >
        {pageSizes.map(pageSize => (
          <Option key={pageSize} value={pageSize}>
            по {pageSize}
          </Option>
        ))}
      </Select>
    </div>
  );
};

export default Pagination;
