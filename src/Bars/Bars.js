import { useRef, useEffect } from "react";
import * as d3 from "d3";

const formatValue = (x) => (isNaN(x) ? "N/A" : x.toLocaleString("en"));

const Bars = ({ width = 400, height = 400, items }) => {
  const ref = useRef();

  useEffect(() => {
    const svg = d3.select(ref.current);
    svg.attr("viewBox", [0, 0, width, height]);
    const margin = { top: 10, right: 10, bottom: 100, left: 40 };
    const data = items.sort((a, b) => b.deprecated - a.deprecated);
    data["columns"] = ["name", "deprecated", "total", "latest"];
    const series = d3
      .stack()
      .keys(data.columns.slice(1))(data)
      // eslint-disable-next-line
      .map((d) => (d.forEach((v) => (v.key = d.key)), d));

    const yAxis = (g) =>
      g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(y).ticks(null, "s"))
        .call((g) => g.selectAll(".domain").remove());

    const xAxis = (g) =>
      g
        .attr("transform", `translate(0,${height - margin.bottom})`)
        .call(d3.axisBottom(x).tickSizeOuter(0))
        .call((g) => g.selectAll(".domain").remove())
        .selectAll("text")
        .attr("transform", `translate(20, 10) rotate(45 0 0)`)

    const y = d3
      .scaleLinear()
      .domain([0, d3.max(series, (d) => d3.max(d, (d) => d[1]))])
      .rangeRound([height - margin.bottom, margin.top]);

    const x = d3
      .scaleBand()
      .domain(data.map((d) => d.name))
      .range([margin.left, width - margin.right])
      .padding(0.1);

    const color = d3
      .scaleOrdinal()
      .domain(series.map((d) => d.key))
      // .range(d3.schemeSpectral[series.length])
      .range([
        "#fbb4ae", // deprecated
        "#b3cde3", // other
        "#ccebc5", // latest
      ])
      .unknown("#ccc");

    svg
      .append("g")
      .selectAll("g")
      .data(series)
      .join("g")
      .attr("fill", (d) => color(d.key))
      .selectAll("rect")
      .data((d) => d)
      .join("rect")
      .attr("x", (d, i) => x(d.data.name))
      .attr("y", (d) => y(d[1]))
      .attr("height", (d) => y(d[0]) - y(d[1]))
      .attr("width", x.bandwidth())
      .append("title")
      .text((d) => `${d.data.name} ${d.key} ${formatValue(d.data[d.key])}`)

    svg.append("g").call(xAxis);
    svg.append("g").call(yAxis);
  }, [width, height, items]);
  return (
    <div>
      <svg className="Bars" width={width} height={height} ref={ref} />
    </div>
  );
};

export default Bars;
