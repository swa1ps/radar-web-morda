const data = [
  {
    name: "service1",
    latest: 1,
    deprecated: 3,
    total: 10
  },
  {
    name: "service2",
    latest: 6,
    deprecated: 3,
    total: 15
  },
  {
    name: "service3",
    latest: 1,
    deprecated: 1,
    total: 12
  },
  {
    name: "service4",
    latest: 1,
    deprecated: 15,
    total: 40
  },
  {
    name: "service5",
    latest: 6,
    deprecated: 5,
    total: 11
  },
  {
    name: "service6",
    latest: 1,
    deprecated: 1,
    total: 21
  },
];

export default data;