import Table from "../Table/Table";
import { Typography, Divider } from "antd";
import { BarCanvas } from "@nivo/bar";
import { Pie } from "@nivo/pie";

import { useState, useMemo } from "react";

const { Title, Link } = Typography;

const Home = ({ languages = [], services = [] }) => {
  const langColumns = [
    {
      Header: "Сервис",
      accessor: "service",
      Cell: (props) => {
        const service = props.cell.value;
        return (
          <Link target="_blank" rel="noreferrer" href={service.href}>
            {service.name}
          </Link>
        );
      },
    },
    {
      Header: "Язык",
      accessor: "language",
    },
    {
      Header: "Версия языка",
      accessor: "langVersion",
    },
    {
      Header: "Версия языка устарела",
      accessor: "deprecated",
    },
    {
      Header: "Последняя версия языка",
      accessor: "latest",
    },
  ];

  const packagesColumns = [
    {
      Header: "Сервис",
      accessor: "service",
      Cell: (props) => {
        const service = props.cell.value;
        return (
          <a target="_blank" rel="noreferrer" href={service.href}>
            {service.name}
          </a>
        );
      },
    },
    {
      Header: "Пакет",
      accessor: "package",
    },
    {
      Header: "Версия пакета",
      accessor: "packageVersion",
    },
    {
      Header: "Язык",
      accessor: "language",
    },
    {
      Header: "Версия языка",
      accessor: "langVersion",
    },
    {
      Header: "Версия пакета устарела",
      accessor: "deprecated",
    },
    {
      Header: "Последняя версия пакета",
      accessor: "latest",
    },
  ];

  let langData = [];
  let packagesData = [];

  services.forEach((service) => {
    let serviceLanguages = {};

    service.languages.forEach((language) => {
      serviceLanguages[language.language_id] = {
        language: language.language,
        language_version: language.language_version,
      };

      langData.push({
        service: {
          name: service.title,
          href: service.repository,
        },
        language: language.language,
        langVersion: language.language_version,
        deprecated: language.is_deprecated ? "да" : "нет",
        latest: language.is_last ? "да" : "нет",
      });
    });
    service.packages.forEach((p) => {
      const languageName = serviceLanguages[p.language_id]?.language ?? ":(";
      const languageVersion =
        serviceLanguages[p.language_id]?.language_version ?? ":(";

      packagesData.push({
        service: {
          name: service.title,
          href: service.repository,
        },
        language: languageName,
        langVersion: languageVersion,
        package: p.package,
        packageVersion: p.package_version,
        deprecated: p.is_deprecated ? "да" : "нет",
        latest: p.is_last ? "да" : "нет",
      });
    });
  });

  let barItems = [];

  services.forEach((service) => {
    let deprecated = 0;
    let latest = 0;

    if (!service.packages.length) return;

    service.packages.forEach(({ is_last, is_deprecated }) => {
      deprecated += is_deprecated;
      latest += is_last;
    });

    barItems.push({
      name: service.title,
      deprecated,
      latest,
      total: service.packages.length,
    });
  });
  barItems.sort((a, b) => b.total - a.total);

  let languageServices = {};


  // eslint-disable-next-line
  let langPieItems = useMemo(() =>
    languages
      .filter(({ versions }) => versions.length)
      .map((l) => {
        let langServicesCount = 0;

        const languageVersions = l.versions.map((v) => {
          const versionServices = v.services.map((service) => {
            const s = services.find((s) => s.id === service.service);
            return {
              id: s.title,
              repository: s.repository,
              value: 1,
              isService: true,
            };
          });

          langServicesCount += v.services.length;

          let el = {
            id: `${l.title}:${v.version}`,
            value: v.services.length,
          };
          if (versionServices.length) {
            el["children"] = versionServices;
          }
          return el;
        });

        languageServices[l.id] = langServicesCount;

        let el = {
          id: l.title,
          value: langServicesCount,
        };
        if (languageVersions.length) {
          el["children"] = languageVersions;
        }
        return el;
      })
  );
  

  const [langPieData, setLangPieData] = useState(langPieItems);
  // eslint-disable-next-line
  let packagesPieItems = useMemo(() => languages
    .filter(({ packages }) => packages.length)
    .map((l) => {

      const languagePackages = l.packages.map((p) => {
        let packageServicesCount = 0;
        const packageVersions = p.versions.map((v) => {
          const versionServices = v.services.map((service) => {
            const s = services.find((s) => s.id === service.service.id || s.id === service);

            return {
              id: s.title,
              repository: s.repository,
              value: 1,
              isService: true,
            };
          });

          packageServicesCount += v.services.length;

          let el = {
            id: `${p.title}:${v.version}`,
            value: v.services.length,
          };
          if (versionServices.length) {
            el["children"] = versionServices;
          }
          return el;
        });
        let el = {
          id: p.title,
          value: packageServicesCount,
        };
        if (packageVersions.length) {
          el["children"] = packageVersions;
        }
        return el;
      });

      let el = {
        id: l.title,
        value: languageServices[l.id],
      };
      if (languagePackages.length) {
        el["children"] = languagePackages;
      }
      return el;
    })
  )

  const [packagesPieData, setPackagesPieData] = useState(packagesPieItems);

  return (
    <div>
      <Title>Радар технологий</Title>
      <h2>Языки</h2>
      <Table columns={langColumns} data={langData} />
      <Pie
        width={1200}
        height={600}
        margin={{ top: 80, right: 120, bottom: 80, left: 120 }}
        sliceLabel={(e) => (e.isService ? "" : e.value)}
        data={langPieData}
        borderWidth={2}
        radialLabelsLinkDiagonalLength={40}
        radialLabelsLinkHorizontalLength={40}
        radialLabelsLinkStrokeWidth={3}
        radialLabelsLinkColor={{ from: "color", modifiers: [] }}
        radialLabelsSkipAngle={2}
        slicesLabelsSkipAngle={2}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        tooltip={(e) =>
          e.isService
            ? `открыть репозиторий ${e.id}`
            : `${e.id}: ${e.value} сервисов`
        }
        onClick={(data, e) => {
          setLangPieData(data.children ? data.children : langPieItems);
          if (data.isService) {
            var win = window.open(data.repository, "_blank");
            win.focus();
          }
        }}
      />
      <Divider />
      <h2>Пакеты</h2>
      <Table columns={packagesColumns} data={packagesData} />
      <Pie
        width={1200}
        height={600}
        margin={{ top: 80, right: 120, bottom: 80, left: 120 }}
        sliceLabel={(e) => (e.isService ? "" : e.value)}
        data={packagesPieData}
        borderWidth={2}
        radialLabelsLinkDiagonalLength={40}
        radialLabelsLinkHorizontalLength={40}
        radialLabelsLinkStrokeWidth={3}
        radialLabelsLinkColor={{ from: "color", modifiers: [] }}
        radialLabelsSkipAngle={2}
        slicesLabelsSkipAngle={2}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        tooltip={(e) =>
          e.isService
            ? `открыть репозиторий ${e.id}`
            : `${e.id}: ${e.value} сервисов`
        }
        onClick={(data, e) => {
          setPackagesPieData(data.children ? data.children : packagesPieItems);
          if (data.isService) {
            var win = window.open(data.repository, "_blank");
            win.focus();
          }
        }}
      />
      <BarCanvas
        width={1200}
        height={2000}
        margin={{ top: 60, right: 150, bottom: 60, left: 200 }}
        data={barItems}
        indexBy="name"
        keys={["deprecated", "total", "latest"]}
        padding={0.2}
        layout="horizontal"
        labelTextColor={"inherit:darker(1.4)"}
        labelSkipWidth={16}
        labelSkipHeight={20}
        tooltip={({ id, value, color }) => (
          <strong style={{ color }}>
            {id}: {value}
          </strong>
        )}
        theme={{
          tooltip: {
            container: {
              background: "#333",
            },
          },
        }}
        colors={{ scheme: "set1" }}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: "Количество пакетов в сервисе",
          legendPosition: "middle",
          legendOffset: 32,
        }}
        legends={[
          {
            dataFrom: "keys",
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            translateX: 120,
            translateY: 0,
            itemsSpacing: 2,
            itemWidth: 100,
            itemHeight: 20,
            itemDirection: "left-to-right",
            itemOpacity: 0.85,
            symbolSize: 30,
            effects: [
              {
                on: "hover",
                style: {
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
      />
    </div>
  );
};

export default Home;
