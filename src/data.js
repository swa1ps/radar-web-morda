const data = {
  name: "sdvor",
  children: [
    {
      "name": "python",
      "children": [
        {
          "name": "django",
          "children": [
            {
              "name": "1.5",
              "value": 1
            },
            {
              "name": "1.9",
              "value": 1
            },
            {
              "name": "2.0",
              "value": 1
            }
          ]
        },
        {
          "name": "pandas",
          "children": [
            {
              "name": "1.5",
              "value": 1
            }
          ]
        }
      ]
    },
    {
      "name": "nodejs",
      "children": [
        {
          "name": "express",
          "children": [
            {
              "name": "3.5",
              "value": 1
            },
            {
              "name": "4.1",
              "value": 1
            }
          ]
        }
      ]
    }
  ]
}

export default data;