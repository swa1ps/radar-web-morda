import "antd/dist/antd.css";
import "./App.css";
import { useState, useEffect } from "react";
import axios from "axios";
import { api } from "./constants";

import { Layout } from "antd";
import Home from "./pages/Home";
import radar from "./radar.gif";
const { Content, Footer } = Layout;

function App() {
  const [languages, setLanguages] = useState([]);
  const [services, setServices] = useState([]);

  useEffect(() => {
    axios.get(`${api}/languages/`).then(function (response) {
      setLanguages(response.data);
    });

    axios.get(`${api}/services/`).then(function (response) {
      setServices(response.data);
    });
  }, []);
  if (!languages.length || !services.length) {
    return (
      <div className="preloader">
        <div className="preloader__title">Сканируем</div>
        <img src={radar} alt=""/>
      </div>
    );
  }

  return (
    <Layout>
      <Content
        className="site-layout"
        style={{
          marginTop: 64,
        }}
      >
        <div
          className="site-layout-background"
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Home languages={languages} services={services} />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>Батин суп ©2020</Footer>
    </Layout>
  );
}

export default App;
